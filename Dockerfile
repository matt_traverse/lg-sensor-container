FROM openwrtorg/rootfs:armvirt-64
RUN mkdir -p /var/lock/opkg/ && opkg update && opkg install shadow-useradd iputils-tracepath iperf3 && useradd -m -s /bin/ash lguser && \
	mkdir /home/lguser/.ssh
RUN chown -R lguser:lguser /home/lguser/.ssh 
RUN uci set dropbear.@dropbear[0].PasswordAuth='off' && \
	uci set dropbear.@dropbear[0].RootPasswordAuth='off'
COPY rc.local /etc/rc.local
